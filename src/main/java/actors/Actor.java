package actors;

import collision.AABB;
import collision.Collision;
import io.Window;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import render.*;
import utils.Log;
import utils.Timer;
import world.World;

import static core.GlobalVariables.*;

/**
 * Created by alex on 24/07/2017.
 * Everything from world to player to enemy is an entity
 */
public abstract class Actor {
    public static final String TAG = "actors.actor";

    private Animation[] animations;
    private int animSize, currAnim;
    protected Transform transform; //makes child able to access it



    //Physics engine stuff
    private boolean isPhysicsEnabled;
    private boolean isGravityEnabled;
    private AABB boundingBox;
    private Vector2f translation;
    private int direction;


    protected boolean isCollidingWithTiles;
    protected boolean enabled;
    protected float fallingSpeed; //Start by 1 and accelerate while falling
    protected float jumpingSpeed;
    protected float runningSpeed;
    protected boolean isOnGround = false;
    protected boolean touchedCeil = false;
    protected boolean resetFallingSpeed = false; //When raised reset falling speed to init value and low itself
    protected boolean isJumping;
    protected boolean isFalling;
    protected boolean stopJumping;

    protected boolean isMovingLeft;
    protected boolean isMovingRight;



    //Debug
    //private boolean changedPos;
    //private Vector3f oldPos;

    public Actor(int maxAnim, Transform transform){
        this.animations = new Animation[maxAnim];
        translation = new Vector2f();
        animSize = 0;
        currAnim = 0;
        this.transform = transform;
        boundingBox = new AABB(new Vector2f(transform.position.x, transform.position.y), new Vector2f(transform.scale.x/2,transform.scale.y/2));//TODO
        isPhysicsEnabled = true;
        isGravityEnabled = true;
        enabled = true;
    }

    public int addAnimation(Animation animation){
        animations[animSize++] = animation;
        return animSize-1;
    }

    public void setAnimation(int i, Animation animation){
        animations[i] = animation;
        animSize++;
    }

    public void selectAnimation(int i){
        currAnim = i;
    }

    public void update(Window window, Camera camera, World world){
        if(enabled){ //not enabled actors do not update
            translation.set(0, 0);

            if(isPhysicsEnabled){ //Firstly follow different behavior with or without physical behaviour enabled
                //Update x axis position;
                moveWithFriction();
                //Update y axis position
                if(isJumping || isFalling){
                    jump();
                }
                if(isGravityEnabled){
                    fallRungeKuttaConstantG();
                }
            } else {
                //TODO: static movement using translation
            }

            applyTranslation(); //Wichever method is used, single moving components are summed into movement array and then applied. Prevent non consistent movements to be computed as a sum of more components
            animationStateMachine();
        }
    }



    /**
     * EULER INTEGRATION
     * https://www.raywenderlich.com/15230/how-to-make-a-platform-game-like-super-mario-brothers-part-1
     * https://www.youtube.com/watch?v=c4b9lCfSDQM
     **/
    void fallEuler(){

        if(isOnGround || resetFallingSpeed){
            fallingSpeed = (float)(Timer.getDelta()*GRAVITY_ACCELERATION);
            jumpingSpeed = MAX_JUMPING_SPEED;
            resetFallingSpeed = false;
        } else {
            float gravityStep = (float) Timer.getDelta()* GRAVITY_ACCELERATION; //Acceleration step with respect of frame rate. a*t obtains a v value
            fallingSpeed+=gravityStep; //Add to current velocity
            fallingSpeed = Math.min(fallingSpeed, MAX_FALLING_SPEED);

            //Log.d(TAG, "delta: " + Timer.getDelta() + ", velocityStep: " + velocityStep + ", fallingSpeed: " + fallingSpeed + ", jumpingSpeed: " + jumpingSpeed);
        }

        float velocityStep = (float)Timer.getDelta()*fallingSpeed; //Velocity step with respect of frame rate. v*t obtains a s value
        velocityStep *= velocityStep;
        //move(new Vector2f(0, -velocityStep));
        translation.add(0, -velocityStep);
    }

    /**
     * RUNGE-KUTTA INTEGRATION
     *   https://www.youtube.com/watch?v=hG9SzQxaCm8
     *   f(t) = 1/2 * g * t^2 + v0 * t + p0; v0 = 2*h/t; g = -2*h/t^2
    **/
    protected void fallRungeKutta(){
        float acc;
        float posOffset = (float)(fallingSpeed * Timer.getDelta() + (0.5f)*GRAVITY_ACCELERATION*(Timer.getDelta()*Timer.getDelta()));
        //update acceleration acc = updateAcceleration(posOffset)
        acc = updateAcceleration(posOffset);
        fallingSpeed += (0.5f)*(GRAVITY_ACCELERATION + acc)*(float)Timer.getDelta();
        GRAVITY_ACCELERATION = acc;
        translation.add(0, posOffset);

    }

    private float updateAcceleration(float posOffset){
        //TODO
        return GRAVITY_ACCELERATION;
    }

    /**
     * RUNGE-KUTTA INTEGRATION simplified
     * 100% accurate if gravity is constant
     * For variable G there's an intrinsic error of dA*dt*dt
     */
    protected void fallRungeKuttaConstantG(){

        if(isOnGround){
            fallingSpeed = (float)(Timer.getDelta()*GRAVITY_ACCELERATION);
            jumpingSpeed = MAX_JUMPING_SPEED;
            isJumping = false;
            isFalling = false;
            //jumpingSpeed = 0;
        } else {
            float posOffset =  (float)(fallingSpeed * Timer.getDelta() + (0.5f)*GRAVITY_ACCELERATION*(Timer.getDelta()*Timer.getDelta()));
            fallingSpeed += GRAVITY_ACCELERATION*(float)Timer.getDelta();
            //Log.d(TAG, "delta: " + Timer.getDelta() + ", posOffset: " + posOffset + ", fallingSpeed: " + fallingSpeed + ", jumpingSpeed: " + jumpingSpeed);
           // move(new Vector2f(0, -posOffset));
            translation.add(0, -posOffset);
        }
    }

    protected void jump(){
        //Log.d(TAG, "jumping");
        Vector2f movement = new Vector2f();
        float posOffset = (float)(jumpingSpeed * Timer.getDelta() + (0.5f)*GRAVITY_ACCELERATION*(Timer.getDelta()*Timer.getDelta()));
        jumpingSpeed-=(float) Timer.getDelta()*GRAVITY_ACCELERATION;
        jumpingSpeed = Math.max(jumpingSpeed, 0);
        if(stopJumping){ //To triple the jumping friction instead of abruptly put it down TODO: this is not accurate. But it is pleasant
            jumpingSpeed-= 2*(float) Timer.getDelta()*GRAVITY_ACCELERATION;
            jumpingSpeed = Math.max(jumpingSpeed, 0);
        }
        movement.add(new Vector2f(0, posOffset));
        isJumping = true;
        //move(movement);

        translation.add(0, posOffset);

        //Bookeping
        if(isOnGround){
            isJumping=false;
            stopJumping=false;
            isFalling = false;
        } else {
            if(!isJumping || (isJumping && touchedCeil)){
                isFalling = true;
                isJumping = false;
                jumpingSpeed = 0;
            }
        }
    }

    /**
     * Check collisions with tile. Returns true if it collides with at least one tile, false otherwise
     * @param world
     * @return
     */
    public boolean checkTileCollision(World world){
        isOnGround = false;
        touchedCeil = false;

        final int RADIUS = 500; //FIXME: make this shit work with low value
        boolean foundCollision = false;

        boundingBox.getCenter().set(transform.position.x, transform.position.y);
        //Log.d(TAG, "check collision. changedPos: " + changedPos);
        AABB[] boxes = new AABB[RADIUS*RADIUS]; //check an area 5x5 around the player
        AABB box = null;
        Vector2f len1, len2;
        Collision data;
        int posX, posY;

        int x, y; //Remind position of colliding tile

        for(int i = 0; i< RADIUS; i++){
            for(int j = 0; j<RADIUS; j++){
                posX = (int)((transform.position.x/2) + 0.5f - (RADIUS/2)) + i;
                posY = (int)((-transform.position.y)/2 + 0.5f - (RADIUS/2)) + j;
                boxes[i +j*RADIUS] = world.getTileBoundingBox(posX, posY);
            }
        }

        for(int i = 0; i< boxes.length; i++){
            if(boxes[i]!=null){ //found a near solid tile
                if(box == null){
                    box = boxes[i]; //init first time

                }
                len1 = box.getCenter().sub(transform.position.x, transform.position.y, new Vector2f());
                len2 = boxes[i].getCenter().sub(transform.position.x, transform.position.y, new Vector2f());

                if(len1.lengthSquared() > len2.lengthSquared()){ //lengthSquared is more CPU friendly
                    box = boxes[i];
                }
            }
        }

        if(box != null){
            data = boundingBox.getCollision(box);
            if(data.isIntersecting){ //Actually adjust position
                //Log.d(TAG, "Colliding with tile at: " + box.getPosition().x + ", " + box.getPosition().y + "; Actor position: " + transform.position.x + ", " + transform.position.y);
                boundingBox.correctPosition(box, data);
                transform.position.set(boundingBox.getCenter(), 0);
                foundCollision = true;

                if(box.getPosition().y > (-transform.position.y) && box.getPosition().x == Math.round(transform.position.x)){ //Check if this touched a tile on the bottom line
                    //Log.d(TAG, "Touched ground!");
                    isOnGround = true;
                } else if(box.getPosition().y < (-transform.position.y) && box.getPosition().x == Math.round(transform.position.x)){ //Check if this touched a tile in the top line
                    touchedCeil = true;
                }
            }
        }

        isCollidingWithTiles = foundCollision;
        return foundCollision;
    }



    public void render(Shader shader, Camera camera, World world){
        Matrix4f target = camera.getProjection();
        target.mul(world.getWorldProjectionMatrix());

        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", transform.getProjection(target));
        //texture.bind();
        animations[currAnim].bind();
        Asset.getModel().render();
    }

    public AABB getBoundingBox() {
        return boundingBox;
    }


    public void move(Vector2f direction){
        move(direction, 1);
    }

    public void move(Vector2f direction, float speed){
        //transform.position.add(new Vector3f(direction.mul(speed), 0));
        translation.add(direction.mul(speed));
    }

    public void moveWithFriction(){

        float posOffset = 0;
        int newDir = 0; // -1 left, 1 right

        if(isMovingLeft){
            newDir = -1;
        } else if (isMovingRight){
            newDir = 1;
        }

        posOffset = (float)(runningSpeed * Timer.getDelta() + (0.5f)*LINEAR_ACCELERATION*(Timer.getDelta()*Timer.getDelta()));

        if(newDir != 0){ //If isMoving, accelerate
            runningSpeed += LINEAR_ACCELERATION*Timer.getDelta();
        } else { //Otherwise slow down

            if(isOnGround){
                runningSpeed -= SHRINKING_FRICTION*(LINEAR_ACCELERATION*Timer.getDelta()); //With friction if on ground
            } else {
                runningSpeed -= VISCOUS_FRICTION *(LINEAR_ACCELERATION*Timer.getDelta()); //With friction if in air
            }
        }

        //Always keep speed between 0 and MAX
        runningSpeed = Math.max(0, runningSpeed);
        runningSpeed = Math.min(MAX_RUNNING_SPEED, runningSpeed);

        //Log.d(TAG, "delta: "+ Timer.getDelta() + ", direction: " + direction + ", newDir: " + newDir  + ", posOffset: " + posOffset + " runningSpeed: " + runningSpeed);
        //transform.position.add(new Vector3f(new Vector2f(posOffset*direction, 0), 0)); //Actually translate the actor

        translation.add(posOffset*direction, 0); //Add the x component to the translation vector

        if(newDir!=0){ //If player want to fastly turn from left to right. If zero mantein old value for 1st law of dynamic
            direction = newDir;
        }
    }

    public void resetMovingSpeed(){
        runningSpeed = 0;
    }

    /**
     * Apply all movement components to the translation
     */
    private void applyTranslation(){
        if(Math.abs(translation.x) <= MIN_THRESHOLD)
            translation.x = 0; //Prevent funny ice skating effect
        if(Math.abs(translation.y) <= MIN_THRESHOLD)
            translation.y = 0;

        //Log.d(TAG, "Refined Translation vector: " + translation.x + ", " + translation.y);
        transform.position.add(new Vector3f(translation, 0));
    }

    /**
     * Movement in the x axis. Negative for left, positive for right
     * @param value
     */
    public void updateMovement(float value){
        translation.add(value, 0);
    }

    public void resetMovement(){
        translation.set(0, 0);
    }

    public void checkActorCollision(Actor other) {
        Collision collision = boundingBox.getCollision(other.getBoundingBox());

        collision.distance.x /=2;
        collision.distance.y /=2;


        if(collision.isIntersecting){
            boundingBox.correctPosition(other.boundingBox, collision);
            transform.position.set(boundingBox.getCenter().x, boundingBox.getCenter().y, 0);

            other.boundingBox.correctPosition(boundingBox, collision);
            other.transform.position.set(other.boundingBox.getCenter().x, other.boundingBox.getCenter().y, 0);
        }
    }

    public abstract int animationStateMachine();

    public boolean isPhysicsEnabled() {
        return isPhysicsEnabled;
    }

    public void setPhysicsEnabled(boolean physicsEnabled) {
        isPhysicsEnabled = physicsEnabled;
    }

    public boolean isGravityEnabled() {
        return isGravityEnabled;
    }

    public void setGravityEnabled(boolean gravityEnabled) {
        isGravityEnabled = gravityEnabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void enable() {
        this.enabled = true;
    }
    public void disable(){
        this.enabled = false;
    }
}
