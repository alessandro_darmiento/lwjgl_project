package actors;


import core.GlobalVariables;
import io.Window;
import org.joml.Vector2f;
import org.joml.Vector3f;
import render.*;
import utils.Timer;
import world.World;

import static core.GlobalVariables.*;
import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by Alex on 20/07/2017.
 */
public class Player extends Actor{
    public static final String TAG = "actors.player";
    public static final int ANIM_SIZE = 3;
    public static int IDLE, RUN, JUMP;

    public Player(Transform transform){
        super(ANIM_SIZE, transform);
        IDLE = addAnimation(new Animation(10, 2, "placeholder"));
        RUN = addAnimation(new Animation(10, 12, "placeholder"));
        JUMP = addAnimation(new Animation(10, 50, "placeholder"));
        selectAnimation(RUN);
        fallingSpeed = 0;
        jumpingSpeed = MAX_JUMPING_SPEED;
    }

    @Override
    public void update(Window window, Camera camera, World world){
        if(enabled){
            //Check collision and apply physics
            super.update(window, camera, world);

            checkInput(window);

            //Make the camera follow player
            camera.getPosition().lerp(transform.position.mul(-world.getScale(), new Vector3f()), GlobalVariables.CAMERA_FRICTION); //smooth
            //camera.setPosition(transform.position.mul(-world.getScale(), new Vector3f())); //not smooth
        }
    }

    /**
     * Override this for custom input
     * Basic input allows to move left-right and jump
     * @return false is no input was received on this frame
     */
    public boolean checkInput(Window window){
        //resetMovement();
        //float d = (float)Timer.getDelta();
        //Vector2f movement = new Vector2f();

        boolean inputReceived = false; //Reset some values
        isMovingLeft = false;
        isMovingRight = false;

        //MOVE left-right
        /*
        if(window.getInputHandler().isKeyDown(GLFW_KEY_A)){
            movement.sub(new Vector2f(10*d, 0));
            inputReceived =  true;
        }

        if(window.getInputHandler().isKeyDown(GLFW_KEY_D)){
            movement.add(new Vector2f(10*d, 0));
            inputReceived = true;
        }
        */

        if(window.getInputHandler().isKeyDown(GLFW_KEY_A)){
            //updateMovement(-10*d);
            isMovingLeft = true;
            inputReceived =  true;
        }

        if(window.getInputHandler().isKeyDown(GLFW_KEY_D)){
            //updateMovement(10*d);
            isMovingRight = true;
            inputReceived = true;
        }

        if(isMovingRight && isMovingLeft){ //Do not accept both
            isMovingRight=false;
            isMovingLeft=false;
        }

        if(window.getInputHandler().isKeyDown(GLFW_KEY_SPACE)){
            if(!stopJumping && !isFalling &&isOnGround){
                isJumping = true;
            }
        }

        if(window.getInputHandler().isKeyReleased(GLFW_KEY_SPACE)){
            if(isJumping && jumpingSpeed>fallingSpeed){
                stopJumping = true;
                resetFallingSpeed = true;
            }

        }
        //move(movement);
        //move();
        //selectAnimation(animationStateMachine(movement));
        return inputReceived;
    }


     public int animationStateMachine(){
        int x = IDLE;
        if (isMovingRight || isMovingLeft)
            x = RUN;
        if (!isOnGround) //jumping overrides running
            x = JUMP;

        return x;
    }
}
