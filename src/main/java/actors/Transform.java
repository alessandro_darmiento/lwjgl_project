package actors;

import core.GlobalVariables;
import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * Created by Alex on 20/07/2017.
 */
public class Transform {
    public Vector3f position;
    public Vector3f scale;
    public Vector3f rotation;

    public Transform(){
        //int s = GlobalVariables.SCALE;
        position = new Vector3f();
        scale = new Vector3f(1,1,1); //Standard scale
        rotation = new Vector3f(1,1,1);
    }

    public Transform(Vector3f scale){
        //int s = GlobalVariables.SCALE;
        position = new Vector3f();
        this.scale = scale;
        rotation = new Vector3f(1,1,1);
    }

    public Matrix4f getProjection(Matrix4f target){
        target.translate(position); //First translate then scale, always!!
        target.scale(scale);
        return target;
    }
}
