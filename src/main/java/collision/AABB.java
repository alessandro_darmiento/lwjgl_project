package collision;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector2i;

/**
 * Created by Alex on 20/07/2017.
 */
public class AABB {
    private Vector2f center, halfExtent;
    private Vector2f upperBound, lowerBound; //points to topLeft and bottoRigt

    private Vector2i position; //World location. Used only in tiles

    public AABB(Vector2f center, Vector2f halfExtent){
        this.center = center;
        this.halfExtent = halfExtent;
        upperBound = center.sub(halfExtent, new Vector2f());
        lowerBound = center.add(halfExtent, new Vector2f());
    }

    public AABB(Vector2f center, Vector2f halfExtent, int x, int y){
        this.center = center;
        this.halfExtent = halfExtent;
        upperBound = center.sub(halfExtent, new Vector2f());
        lowerBound = center.add(halfExtent, new Vector2f());
        position = new Vector2i(x, y);
    }

    //https://gamedevelopment.tutsplus.com/tutorials/basic-2d-platformer-physics-part-1--cms-25799
    public Collision getCollision(AABB other) {
        Vector2f distance = other.center.sub(center, new Vector2f());
        distance.x = (float) Math.abs(distance.x);
        distance.y = (float) Math.abs(distance.y);

        distance.sub(halfExtent.add(other.halfExtent, new Vector2f())); //Actual distance

        //return new Collision(distance, distance.x < 0 && distance.y < 0);
        //return new Collision(distance, distance.x < 0 && distance.y < 0, (lowerBound.y - other.upperBound.y)>= 0.05);
        if (center.y > other.center.y) {

        } else {

        }
        return new Collision(distance, distance.x < 0 && distance.y < 0);
    }

    public void correctPosition(AABB other, Collision data){
        Vector2f correction = other.center.sub(center, new Vector2f());
        if(data.distance.x > data.distance.y){
            if(correction.x > 0){
                center.add(data.distance.x, 0);
            } else{
                center.add(-data.distance.x, 0);
            }
        } else {
            if(correction.y > 0){
                center.add(0, data.distance.y);
            } else{
                center.add(0, -data.distance.y);
            }

        }
    }

    public Vector2f getCenter() {
        return center;
    }

    public Vector2f getHalfExtent() {
        return halfExtent;
    }

    public Vector2i getPosition() {
        return position;
    }
}
