package collision;

import org.joml.Vector2f;

/**
 * Created by Alex on 20/07/2017.
 */
public class Collision {
    public Vector2f distance;
    public boolean isIntersecting;

    public boolean AtopB;
    public boolean BtopA;
    public boolean AleftB;
    public boolean BleftA;


    public Collision(Vector2f distance, boolean isIntersecting){
       this.distance = distance;
       this.isIntersecting = isIntersecting;
    }

    public Collision(Vector2f distance, boolean isIntersecting, boolean AtopB, boolean BtopA, boolean AleftB, boolean BleftA){
        this.distance = distance;
        this.isIntersecting = isIntersecting;
        this.AtopB = AtopB;
        this.BtopA = BtopA;
        this.AleftB = AleftB;
        this.BleftA = BleftA;
    }
}
