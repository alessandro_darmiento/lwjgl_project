package core;

import render.Camera;
import render.Model;
import render.Shader;
import render.Texture;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Alex on 18/07/2017.
 */
@Deprecated
public class Core {
    Random random;

    private static Core mInstance;
    private Camera camera;

    private Core() {
        random = new Random();
        camera = new Camera(640, 480);
    }

    public static synchronized Core getInstance() {
        if (null == mInstance)
            mInstance = new Core();
        return mInstance;
    }




    //Load a gradient background color and basic texture
    public void loadBackground() {

        Texture texture = createTexture();
        texture.bind();


        glBegin(GL_QUADS);
        glColor4d(0.6, 0.7, 0.9, 0);

        glVertex2d(-1, 1);
        glTexCoord2f(0, 0);

        glVertex2d(1, 1);
        glTexCoord2f(0, 1);
        //glVertex2d(GlobalVariables.S_W/2, GlobalVariables.S_H/2);
        glColor4d(0.2, 0.4, .8, 0);
        //glVertex2d(GlobalVariables.S_W/2, -GlobalVariables.S_H/2);
        //glVertex2d(-GlobalVariables.S_W/2, -GlobalVariables.S_H/2);
        glVertex2d(1, -1);
        glTexCoord2f(1, 1);

        glVertex2d(-1, -1);
        glTexCoord2f(1, 0);
        glEnd();

    }
    Matrix4f scale, target;
    Model model;
    Texture texture;

    public void initCamera(){
        camera.getProjection().scale(128);

        scale = new Matrix4f().translate(new Vector3f(0, 0, 0)).scale(128);
        target = new Matrix4f();
        float vertices[] = new float[]{
                -0.5f, 0.5f, 0,
                0.5f, 0.5f, 0,
                0.5f, -0.5f, 0,
                -0.5f, -0.5f, 0,
        };

        texture = createTexture();


        float[] textureCoord = new float[]{
                0,0,
                1,0,
                1,1,
                0,1,

        };

        int[] indices = new int[]{
                0, 1, 2,
                2, 3, 0
        };

        model = new Model(vertices, textureCoord, indices);
    }

    public void renderPoly(){
        //Matrix4f scale = new Matrix4f().translate(new Vector3f(100, 0, 0)).scale(128);
        //Matrix4f target = new Matrix4f();

        //target = scale;

        Shader shader = new Shader("shader");
        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", camera.getProjection());
        texture.bind(0);
        model.render();
    }



    public Texture createTexture() {
        return new Texture("placeholder.jpg");
    }
}
