package core;

/**X
 * Created by Alex on 18/07/2017.
 */
public class GlobalVariables {
    public static final String APP_NAME = "Pico2D";

    //Render
    public static final int S_H = 800;
    public static final int S_W = 600;
    public static final int SCALE = 64;
    public static int RENDER_DISTANCE_X = 15; //base value
    public static int RENDER_DISTANCE_Y = 11;
    public static final double FRAME_CAP = 1.0/50.0; //1 sec 60 frames. 16.7 ms

    //Paths
    public static final String RES_PATH = "src/main/resources/";
    public static final String TEX_PATH = RES_PATH + "textures/";
    public static final String SHA_PATH = RES_PATH + "shaders/";
    public static final String LVL_PATH = RES_PATH + "levels/";
    public static final String TLS_PATH = "tilesets/";

    //Physics
    public static float CAMERA_FRICTION = 0.06f;
    public static float GRAVITY_ACCELERATION = 19f;
    public static final float MAX_FALLING_SPEED = 26f;
    public static final float MAX_JUMPING_SPEED = 18f;
    public static final float MAX_RUNNING_SPEED = 10f;
    public static final float LINEAR_ACCELERATION = 30f;
    public static final float SHRINKING_FRICTION = 4f;
    public static final float VISCOUS_FRICTION = 0.5f;

    public static final float MIN_THRESHOLD = 0.01f; //Values below this are forced to zero
    
}
