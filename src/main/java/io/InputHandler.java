package io;

import utils.Log;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by Alex on 19/07/2017.
 * Polling to check for each key if it is pressed
 */
public class InputHandler {
    public static final String TAG = "io.inputhandler";
    private long window;
    private boolean keys[];
    private boolean mouseButtons[];

    public InputHandler(long window){
        this.window = window;
        this.keys = new boolean[GLFW_KEY_LAST]; //kept a hole of 31 boolean for practical reason
        for(int i = 0; i<keys.length;i++) keys[i] = false;
    }

    public boolean isKeyDown(int key){
        return glfwGetKey(window, key) == 1;
    }

    public boolean isKeyPressed(int key){
        return (isKeyDown(key) && !keys[key]);
    }


    public boolean isKeyReleased(int key){
        return (!isKeyDown(key) && keys[key]);
    }

    public boolean isMouseButtonDown(int button){
        return glfwGetMouseButton(window, button) == 1;
    }

    public void update(){
        for(int i = 32; i<keys.length;i++){ //dumb gl inventor doesn't know how to count below 32 took me forever to guess
            keys[i] = isKeyDown(i);
        }
    }


}
