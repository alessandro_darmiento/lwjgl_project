package io;


import core.GlobalVariables;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import io.InputHandler;
import org.lwjgl.glfw.GLFWWindowSizeCallback;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by Alex on 19/07/2017.
 */
public class Window {
    private GLFWVidMode vidMode;
    private InputHandler inputHandler;

    private long window;
    private int width;
    private int height;
    private boolean fullScreen;

    private boolean resized;
    private GLFWWindowSizeCallback windowSizeCallback;


    public static void setCallback(){
        glfwSetErrorCallback(GLFWErrorCallback.createPrint(System.err));
    }

    public Window(){
        setSize(800, 600);
        setFullScreen(false); //TODO remove
        resized = false;
    }

    public void createWindow() throws IllegalStateException{
        window = org.lwjgl.glfw.GLFW.glfwCreateWindow(
                width,
                height,
                GlobalVariables.APP_NAME,
                fullScreen?glfwGetPrimaryMonitor() : 0,
                0);

        if(0==window){
            throw new IllegalStateException("Failed to create window");
        }
        if(!fullScreen){
            vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor()); //information about monitor
            glfwSetWindowPos(
                    window,
                    (vidMode.width() - width)/2,
                    (vidMode.height()/height)/2);
        }

        glfwMakeContextCurrent(window);
        glfwSwapInterval(1); // Enable v-sync

        glfwShowWindow(window);

        inputHandler = new InputHandler(window);
        setLocalCallback(); //for the resize callback
    }

    public void setSize(int width, int height){
        this.width=width;
        this.height=height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean shouldClose(){
        return glfwWindowShouldClose(window);
    }

    public void swapBuffer(){
        glfwSwapBuffers(window);
    }

    public boolean isFullScreen() {
        return fullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        this.fullScreen = fullScreen;
    }
    public long getWindowTEMP(){
        return window;
    }

    public InputHandler getInputHandler(){
        return inputHandler;
    }

    public void update(){
        resized = false;
        inputHandler.update();

        glfwPollEvents();
    }

    public boolean hasResized(){
        return resized;
    }


    private void setLocalCallback(){
        windowSizeCallback = new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long win, int w, int h) {
                width = w;
                height = h;

                resized = true;
            }
        };
        glfwSetWindowSizeCallback(window, windowSizeCallback);
    }

    public void free(){ //Free memory allocated data
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);
    }
}
