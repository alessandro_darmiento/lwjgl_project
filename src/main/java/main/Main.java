package main;

/**
 * Created by Alex on 18/07/2017.
 */


import core.GlobalVariables;
import io.Window;
import org.lwjgl.*;
import org.lwjgl.opengl.*;
import render.*;
import utils.Log;
import utils.Timer;
import world.World;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;


public class Main {
    public static final String TAG = "main.main";
    private Window window;
    private Camera camera;
    private TileRenderer tileRenderer;
    private long totalFrames =0;

    //TEST
    World world;
    Shader shader;
    //GUI gui;


    public void run() {
        System.out.println("Hello LWJGL " + Version.getVersion() + "!");

        init();
        loop();

        //Free the window callbacks and destroy the window
        window.free();

        //Delete the model uded for every actor
        Asset.deleteAsset();

        // Terminate GLFW and free the error callback
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    private void init() {

        // Setup an error callback.
        Window.setCallback();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        //Create Window
        window = new Window();
        //window.setSize(800, 600);
        window.setFullScreen(false);
        window.createWindow();

        //Setup GL stuff
        GL.createCapabilities(); //TODO: what's this shit?
        glEnable(GL_TEXTURE_2D); //For 2D textures
        glEnable(GL_BLEND); //For transparency
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Also for transparency

        //Init camera and renderer
        camera = new Camera(window.getWidth(), window.getHeight());
        tileRenderer = new TileRenderer();
        Asset.initAsset();

        //Init test stuff
        shader = new Shader("shader");
        //world = new World(camera);
        world = new World("test", camera);
        world.computeViewDistance(window);
        //gui = new GUI(window);


    }

    private void loop() {

        double time = Timer.getTime();
        double unprocessed = 0;
        double frameTime=0;
        int frames=0;

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!window.shouldClose()) {

            boolean canRender = false;
            double time2 = Timer.getTime();
            double passed = time2 - time;
            //Log.d(TAG, "" + passed);
            unprocessed += passed;
            frameTime+=passed;
            time = time2;

            while (unprocessed >= GlobalVariables.FRAME_CAP) {

                if(window.hasResized()){
                    world.computeViewDistance(window);
                    glViewport(0, 0, window.getWidth(), window.getHeight());
                    camera.setProjection(window.getWidth(), window.getHeight());
                    //gui.resizeCamera(window);
                }


                unprocessed -= GlobalVariables.FRAME_CAP;
                canRender = true;

                if(window.getInputHandler().isKeyPressed(GLFW_KEY_ESCAPE)){
                    glfwSetWindowShouldClose(window.getWindowTEMP(), true);
                }

                //player.update(window, camera, world);
                world.update(window);
                world.correctCamera(window);
                window.update();

                if(frameTime>=1.0){
                    frameTime=0;
                    Log.d(TAG, "FPS: " + frames);
                    frames = 0;
                }
            }

            if(canRender){
                frames++;
                totalFrames++;
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
                world.render(tileRenderer, shader, window);
                //gui.render();
                window.swapBuffer(); // swap the color buffers
                Timer.update();
            }

        }
    }

    public static void main(String[] args) {
        Log.d(TAG,"Working Directory = " +
                System.getProperty("user.dir"));
        new Main().run();
    }

}