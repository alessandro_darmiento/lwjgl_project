package render;

import utils.Timer;

/**
 * Created by Alex on 20/07/2017.
 */
public class Animation {
    private Texture[] frames;
    private int pointer; //index in the array of frames
    private double time, currentTime, lastTime, elapsedTime, fps; //Framerate limiters

    public Animation(int amount, int fps, String filename){
        this.pointer = 0;
        this.elapsedTime = 0;
        this.currentTime = 0;
        this.time = 0;
        this.lastTime = Timer.getTime();
        this.fps = 1.0/(double)fps;

        this.frames = new Texture[amount];
        for(int i = 0; i<amount;i++){
            frames[i] = new Texture("anim/" + filename + "/" + i + ".png");
        }
    }

    public void bind(){
        bind(0);
    }

    public void bind(int sampler){
        this.currentTime = Timer.getTime();
        this.elapsedTime += currentTime - lastTime;

        if(elapsedTime>=fps){
            elapsedTime = 0;
            pointer++;
        }

        if(pointer>=frames.length){
            pointer = 0;
        }

        this.lastTime = currentTime;

        frames[pointer].bind(sampler);
    }
}
