package render;

/**
 * Created by alex on 24/07/2017.
 * A static model for everyone
 */
public class Asset {
    public static final String TAG = "render.asset";

    //Model is static because in sprite based games every model is a 2 triangles square
    //Having it static allows to keep just one copy on VGA mem
    private static Model model;

    public static void initAsset(){
        float vertices[] = new float[]{
                -0.5f, 0.5f, 0, //Top left corner
                0.5f, 0.5f, 0,  //Top right corner
                0.5f, -0.5f, 0, //Bottom left corner
                -0.5f, -0.5f, 0 //Bottom right corner
        };

        float[] textureCoord = new float[]{
                0,0,
                1,0,
                1,1,
                0,1,

        };

        int[] indices = new int[]{
                0, 1, 2, //Pointers to vertices coord
                2, 3, 0
        };

        model = new Model(vertices, textureCoord, indices);
    }

    public static void deleteAsset(){
        model = null;
    }

    public static Model getModel(){
        return model;
    }

}
