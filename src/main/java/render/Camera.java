package render;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import utils.Log;

/**
 * Created by Alex on 19/07/2017.
 */
public class Camera {
    public final static String TAG = "render.camera";
    private Vector3f position;
    private Matrix4f projection;

    public Camera(int width, int height){
        position = new Vector3f(0,0,0);
        setProjection(width, height);
        //projection = new Matrix4f().setOrtho2D(-width/2, width/2, -height/2, height/2);
    }

    public void setProjection(int width, int height){
        projection = new Matrix4f().setOrtho2D(-width/2, width/2, -height/2, height/2);
    }

    public void setPosition(Vector3f position){
        this.position = position;
    }

    public void addPosition(Vector3f offset){
        this.position.add(offset);
        //Log.d(TAG, "Camera position: " + this.position);
    }

    public Vector3f getPosition(){
        return position;
    }

    public Matrix4f projection(){
        Matrix4f target = new Matrix4f();
        Matrix4f pos = new Matrix4f().setTranslation(position);
        target = projection.mul(pos, target);

        return target;
    }

    public Matrix4f getProjection() {
        return projection.translate(position, new Matrix4f());
    }
    public Matrix4f getUntrasformedProjection() {
        return projection;
    }

}
