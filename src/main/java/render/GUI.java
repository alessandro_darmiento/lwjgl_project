package render;

import core.GlobalVariables;
import io.Window;
import org.joml.Matrix4f;
import org.joml.Vector4f;


/**
 * Created by alex on 24/07/2017.
 */
public class GUI {
    private static final String TAG = "render.gui";

    private Shader shader;
    private Camera camera;
    private TileSet set;



    public GUI(Window window){
        shader = new Shader("gui");
        this.camera = new Camera(window.getWidth(), window.getHeight());
        set = new TileSet("newMario.png", 16);
    }

    public void resizeCamera(Window window){
        camera.setProjection(window.getWidth(), window.getHeight());
    }


    public void render(){
        Matrix4f tmp = new Matrix4f();
        camera.getUntrasformedProjection().scale(GlobalVariables.SCALE, tmp);
        tmp.translate(-1, -1, 0);
        shader.bind();

        shader.setUniform("projection", tmp);
        //shader.setUniform("color", new Vector4f(0,0,0,0.4f));
        set.bindTile(shader, 49);
        Asset.getModel().render();
    }
}
