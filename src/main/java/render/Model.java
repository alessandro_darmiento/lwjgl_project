package render;

import org.lwjgl.BufferUtils;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;


/**
 * Created by Alex on 19/07/2017.
 */

public class Model {
    public static final String TAG = "render.model";

    private int drawCount;
    private int vId; //vertexId
    private int tId; //textureId
    private int iId; //indexId

    public Model(float[] vertices, float[] textureCoords, int[] indices){
        drawCount = indices.length; //Amount of vertices to drawn

        //Vertices
        vId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vId); //Binding vId var to the array buffer
        glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(vertices), GL_STATIC_DRAW); //STATIC pass once, DYNAMIC if change

        //Texture
        tId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(textureCoords), GL_STATIC_DRAW);

        //Index
        iId = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iId);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, createIntBuffer(indices), GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0); //Unbind buffers, for safeness
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    }


    public void render(){
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glEnableClientState(GL_TEXTURE_COORD_ARRAY);

        //Vertices
        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

        //Texture
        glBindBuffer(GL_ARRAY_BUFFER,tId);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

        //Indeces
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iId);

        glDrawElements(GL_TRIANGLES, drawCount, GL_UNSIGNED_INT, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0); //for safeness

        glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);
    }

    private FloatBuffer createFloatBuffer(float[] data){
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }



    private IntBuffer createIntBuffer(int[] data){
        IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    protected void finalize() throws Throwable{
        glDeleteBuffers(vId);
        glDeleteBuffers(tId);
        glDeleteBuffers(iId);
        //super.finalize();
    }

}
