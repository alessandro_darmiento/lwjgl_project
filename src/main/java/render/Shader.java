package render;

/**
 * Created by Alex on 19/07/2017.
 */

import core.GlobalVariables;
import org.joml.Matrix4f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;
import utils.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

public class Shader {
    public static final String TAG = "render.shader";

    private int program;
    private int vertexShader;
    private int fragmentShader;

    public Shader(String filename){
        //Log.d(TAG, "Init " + filename);
        String filePath = GlobalVariables.SHA_PATH + filename;
        String vsPath = filePath + ".vs";
        String fsPath = filePath + ".fs";

        if( !(new File(vsPath).exists() )){
            Log.wtf(TAG, "Wrong filename: " + filename);
        }
        if( !(new File(fsPath).exists() )){
            Log.wtf(TAG, "Wrong filename: " + filename);
        }

        program = glCreateProgram();
        //Ciao

        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, readFile(vsPath));
        glCompileShader(vertexShader);
        if(glGetShaderi(vertexShader, GL_COMPILE_STATUS) != 1){
            Log.wtf(TAG, "Cannot compile vertex shader " + filename + " " + glGetShaderInfoLog(vertexShader));
        }

        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, readFile(fsPath));
        glCompileShader(fragmentShader);
        if(glGetShaderi(fragmentShader, GL_COMPILE_STATUS) != 1){
            Log.wtf(TAG, "Cannot compile fragment shader " + filename + " " + glGetShaderInfoLog(fragmentShader));
        }

        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);

        glBindAttribLocation(program, 0, "vertices");
        glBindAttribLocation(program, 1, "textures");

        glLinkProgram(program);
        if(glGetProgrami(program, GL_LINK_STATUS)!=1){
            Log.wtf(TAG, "Cannot link program " + filename + " " + glGetProgramInfoLog(program));
        }

        glValidateProgram(program);
        glLinkProgram(program);
        if(glGetProgrami(program, GL_VALIDATE_STATUS)!=1){
            Log.wtf(TAG, "Cannot validate program " + filename + " " + glGetProgramInfoLog(program));
        }

    }

    private String readFile(String path){
        if( !(new File(path).exists() )){ //Redundant check. TODO: remove one of these
            Log.wtf(TAG, "Wrong filename: " + path);
        }
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader;
        String line;
        try{
            bufferedReader = new BufferedReader(new FileReader(new File(path)));
            while((line = bufferedReader.readLine()) != null){
                sb.append(line);
                sb.append("\n");
            }
            bufferedReader.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return sb.toString();
    }

    public void setUniform(String name, float value){
        int location = glGetUniformLocation(program, name);
        if(-1 == location){
            Log.wtf(TAG, "Invalid uniform " + name );
            return;
        }
            glUniform1f(location, value);
    }

    public void setUniform(String name, Matrix4f value){
        int location = glGetUniformLocation(program, name);
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        value.get(buffer);

        if(-1 == location){
            Log.wtf(TAG, "Invalid uniform " + name );
            return;
        }
        glUniformMatrix4fv(location,false, buffer);
    }

    public void setUniform(String name, Vector4f value){
        int location = glGetUniformLocation(program, name);

        if(-1 == location){
            Log.wtf(TAG, "Invalid uniform " + name );
            return;
        }
        glUniform4f(location,value.x, value.y, value.z, value.w);
    }

    public void bind(){
        glUseProgram(program);
    }



    protected void finalize(){
        glDetachShader(program, vertexShader);
        glDetachShader(program, fragmentShader);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        glDeleteProgram(program);
    }

}
