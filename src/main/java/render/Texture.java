package render;

import core.GlobalVariables;
import org.lwjgl.BufferUtils;
import utils.Log;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

/**
 * Created by Alex on 19/07/2017.
 */
public class Texture {
    public static final String TAG = "render.texture";

    private int id;
    private int width;
    private int height;

    public Texture(String filename){
        BufferedImage bi;
        String filePath = GlobalVariables.TEX_PATH + filename;
        if( !(new File(filePath).exists() )){
            Log.wtf(TAG, "Wrong filename: " + filePath);
        }

        try{
            bi = ImageIO.read(new File(filePath));
            width = bi.getWidth();
            height = bi.getHeight();
            int[] pixelMap = new int[width*height]; //32 bit integer for rgba 4 bytes
            pixelMap = bi.getRGB(0, 0, width, height, null, 0, width);
            ByteBuffer pixels = BufferUtils.createByteBuffer(width*height*4);

            for(int i =0; i<height; i++){
                for(int j =0; j<width;j++){
                    int pixel = pixelMap[i*width + j];
                    pixels.put((byte)((pixel >> 16) & 0xFF)); //r
                    pixels.put((byte)((pixel >> 8) & 0xFF));  //g
                    pixels.put((byte)(pixel & 0xFF));         //b
                    pixels.put((byte)((pixel >> 24) & 0xFF)); //a
                }
            }

            pixels.flip();

            id = glGenTextures();

            glBindTexture(GL_TEXTURE_2D, id);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

            //TODO

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void bind(){
        bind(0);
    }

    public void bind(int sampler){
        if(sampler >=0 && sampler <31)
            glActiveTexture(GL_TEXTURE0 + sampler); //Bind to first sampler
        glBindTexture(GL_TEXTURE_2D, id);
    }


    protected void finalize() throws Throwable{
        glDeleteTextures(id);
        super.finalize();
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
