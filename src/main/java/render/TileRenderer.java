package render;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import world.Tile;

import java.util.HashMap;

/**
 * Created by Alex on 19/07/2017.
 */
public class TileRenderer {
    private HashMap<String, Texture> textures;

    public TileRenderer(){
        textures = new HashMap<>();

        for(int i = 0; i<Tile.tiles.length;i++){
            String str;
            if(Tile.tiles[i] != null){
                if(!textures.containsKey(Tile.tiles[i].getTexture())){
                    str = Tile.tiles[i].getTexture();
                    textures.put(str, new Texture(str + ".png"));
                }
            }
        }
    }


    public void renderTile(Tile tile, int x, int y, Shader shader, Matrix4f world, Camera camera){
        Matrix4f tilePosition = new Matrix4f().translate(new Vector3f(x, y, 0));
        Matrix4f target = new Matrix4f();

        shader.bind();
        if(textures.containsKey(tile.getTexture())){
            textures.get(tile.getTexture()).bind(0);
        }

        camera.getProjection().mul(world, target); //camera * world and then put into target
        target.mul(tilePosition);

        shader.setUniform("sampler", 0);
        shader.setUniform("projection", target);

        Asset.getModel().render();

    }

}
