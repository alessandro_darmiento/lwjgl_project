package render;

import org.joml.Matrix4f;
import static core.GlobalVariables.*;

public class TileSet {
    private Texture texture;
    private Matrix4f scale;
    private Matrix4f tranlsation;

    private int qty;


    public TileSet(String texture, int qty){
        this.texture = new Texture(TLS_PATH + texture);
        scale = new Matrix4f().scale(1.0f/(float)qty);
        tranlsation = new Matrix4f();
        this.qty = qty;
    }

    public void bindTile(Shader shader, int x, int y){
        scale.translate(x, y, 0, tranlsation);

        shader.setUniform("sampler", 0);
        shader.setUniform("texModifier", tranlsation);
        texture.bind(0);
    }

    public void bindTile(Shader shader, int tile){
        int x = tile%qty;
        int y = tile/qty;
        bindTile(shader, x, y);
    }

}
