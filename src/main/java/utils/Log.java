package utils;

/**
 * Created by Alex on 19/07/2017.
 */
public class Log {
    public static void d(String TAG, String message){
        System.out.println(TAG + ": " + message);
    }
    public static void wtf(String TAG, String message){
        System.err.println(TAG + ": " + message);
    }
}
