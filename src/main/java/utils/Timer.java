package utils;

/**
 * Created by Alex on 19/07/2017.
 */
public class Timer {
    public static final String TAG = "utils.timer";
    private static double delta=0;

    private static double time1=-1, time2=-1;



    public static double getTime(){

        return (double) System.nanoTime()/(double)1000000000L;
    }

    public static void update(){
        if(time1 == -1)
            time1 = getTime();
        time2 = getTime();
        delta = time2 - time1;
        time1 = time2;
        //Log.d(TAG, "" + delta);
    }

    public static void setDelta(double dt){
        if (dt > 0.02f){ //Prevent funny behaviour if fps drops
            dt = 0.02f;
            Log.d(TAG, "Unusual dt value: " + dt);
        }
        delta = dt;
    }

    public static double getDelta(){
        return delta;
    }

}
