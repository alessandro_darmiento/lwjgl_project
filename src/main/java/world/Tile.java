package world;

/**
 * Created by Alex on 19/07/2017.
 */
public class Tile {
    public final static String TAG = "world.tile";
    public static Tile tiles[] = new Tile[4096]; //max 4096
    public static byte numOfTiles =0;

    public static final Tile testTile = new Tile("placeholder");
    public static final Tile testTile2 = new Tile("mario_placeholder").setSolid(true);

    private byte id;
    private String texture;

    private boolean solid;



    public Tile(String texture){
        this.id = numOfTiles;
        this.solid = false; //not solid by default
        numOfTiles++;

        this.texture=texture;

        if(tiles[id] != null){
            throw new IllegalStateException("Tiles at " + id + " already in use");
        }
        tiles[id] = this;

    }

    public static Tile[] getTiles() {
        return tiles;
    }


    public byte getId() {
        return id;
    }

    public String getTexture() {
        return texture;
    }

    public boolean isSolid() {
        return solid;
    }

    public Tile setSolid(boolean solid) {
        this.solid = solid;
        return this;
    }
}
