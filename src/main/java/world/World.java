package world;

import actors.Actor;
import actors.Player;
import actors.Transform;
import collision.AABB;
import core.GlobalVariables;
import io.Window;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL;
import render.Camera;
import render.Shader;
import render.TileRenderer;
import utils.Log;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Alex on 20/07/2017.
 */
public class World {
    public final static String TAG = "world.world";
    private Matrix4f world;
    private Camera camera;
    private byte[] tiles;
    private int width;
    private int height;

    private AABB[] boundingBoxes;
    private ArrayList<Actor> actors;
    private Player player;

    @Deprecated //Does not support tileset
    public World(Camera camera){
        this.camera = camera;
        width= 32;
        height= 32;

        tiles = new byte[width*height];

        world = new Matrix4f().setTranslation(new Vector3f(0));
        world.scale(GlobalVariables.SCALE);
        boundingBoxes = new AABB[width*height];
    }


    public World(String levelName, Camera camera){
        Tile t;
        BufferedImage tileSet, actorSet;
        Transform transform;
        String tilesPath = GlobalVariables.LVL_PATH  + levelName + "/tileset.png";
        String actorsPath = GlobalVariables.LVL_PATH + levelName + "/actor.png";

        if(!(new File(tilesPath).exists()) || !(new File(actorsPath).exists())){
            Log.wtf(TAG, "Wrong filename!");
        }

        world = new Matrix4f().setTranslation(new Vector3f(0)); //world projection matrix
        world.scale(GlobalVariables.SCALE);
        this.camera = camera;
        actors = new ArrayList<>();


        try{
            tileSet = ImageIO.read(new File(tilesPath));
            actorSet = ImageIO.read(new File(actorsPath));

            width = tileSet.getWidth();
            height = tileSet.getHeight();


            int[] colorTileSet = tileSet.getRGB(0,0, width, height,null,
                    0, width); //returns all the pixels within the image

            int[] colorActorSet = actorSet.getRGB(0, 0, width, height, null,
                    0, width);


            tiles = new byte[width*height];
            boundingBoxes = new AABB[width*height];

            for(int i = 0; i<height; i++){
                for(int j= 0; j<width; j++){
                    int red = (((colorTileSet[j + i*width]) >>16) & 0xFF); //read the red byte of the int of colorTileSet
                    int actorIndex =  (((colorActorSet[j + i*width]) >>16) & 0xFF);
                    int alphaChannel = (((colorActorSet[j + i*width]) >>24) & 0xFF);

                    try{
                        t = Tile.tiles[red]; //throws exception
                        //Log.d(TAG, "t "+ i + "," +j + " value: " + red);
                    } catch (IndexOutOfBoundsException e){
                        Log.wtf(TAG, "Trying to allocate more than 256 different tiles");
                        e.printStackTrace();
                        t = null;
                    }
                    if(null != t){
                        setTile(t, j, i); //actually set the tile
                    }

                    if(alphaChannel > 0){
                        transform = new Transform();
                        transform.position.x = j;
                        transform.position.y = -i;
                        switch(actorIndex){
                            case 1: //Player
                                Player p = new Player(transform);
                                actors.add(p);
                                player = p;
                                camera.setPosition(transform.position.mul(-GlobalVariables.SCALE, new Vector3f())); //not smooth
                                break;
                            default: //Others

                                break;
                        }
                    }

                }
            }

        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void computeViewDistance(Window window){
        GlobalVariables.RENDER_DISTANCE_X = window.getWidth()/GlobalVariables.SCALE + 8;
        GlobalVariables.RENDER_DISTANCE_Y = window.getHeight()/GlobalVariables.SCALE + 8;

    }

    /**
     * @param renderer
     * @param shader
     * Render only the portion under camera focus. TODO:tweak values
     */
    public void render(TileRenderer renderer, Shader shader, Window window){
        final int scale = GlobalVariables.SCALE;


        int posX = ((int)camera.getPosition().x + window.getWidth()/2)/scale;
        int posY = ((int)camera.getPosition().y - window.getHeight()/2)/scale;

        //int posX = ((int)camera.getPosition().x)/scale;
        //int posY = ((int)camera.getPosition().y)/scale;

        for(int i=0;i < GlobalVariables.RENDER_DISTANCE_X; i++){
            for(int j = 0; j<GlobalVariables.RENDER_DISTANCE_Y; j++){
                Tile t = getTile(i - posX, j + posY);
                //Tile t = getTile(i - posX - GlobalVariables.RENDER_DISTANCE_X +1, j + posY + GlobalVariables.RENDER_DISTANCE_Y);

                if(t != null){
                    renderer.renderTile(t, i-posX, -j-posY, shader, world, camera);
                }
            }

        }

        for(Actor a : actors){
            a.render(shader, camera, this);
        }

    }


    public void update(Window window){
        for(Actor a : actors){
            a.update(window, camera, this);
        }

        for(int i = 0; i< actors.size(); i++){
            actors.get(i).checkTileCollision(this); //check if collides with tileObjects
            //actors.get(i).checkTileCollision(this); //check if collides with tileObjects

            for(int j = i+1; j < actors.size(); j++){
                actors.get(i).checkActorCollision(actors.get(j)); //check if collides with j actor
            }
        }

    }

    /**
     * Camera projection matrix adjustment
     * Prevent camera from stepping outside the world
     * @param window
     */
    public void correctCamera(Window window){
        final int scale = GlobalVariables.SCALE;
        Vector3f pos = camera.getPosition();
        int w = -width*scale;
        int h = height*scale;

        //Left side
        if(pos.x > -(window.getWidth()/2) + scale/2){ //Add scale because it causes a translation
            pos.x = -window.getWidth()/2 + scale/2;
        }
        //Right side
        if(pos.x < w + (window.getWidth())/2 + scale/2 ){
            pos.x = w + (window.getWidth())/2 + scale/2;
        }
        //Top side
        if(pos.y < (window.getHeight() - scale)/2){
            pos.y = (window.getHeight() - scale)/2;
        }
        //Bottom side
        if(pos.y > h - (window.getHeight() + scale)/2){
            pos.y = h - (window.getHeight() + scale)/2;
        }

    }

    public void setTile(Tile tile, int x, int y){ //TODO: check x y?
        tiles[x + y*width] = tile.getId();
        if(tile.isSolid()){
            //Log.d(TAG, "Creating solid tile at " + x + " " + (-y));
            boundingBoxes[x + y*width] = new AABB(new Vector2f(x, -y), new Vector2f(0.5f, 0.5f), x, y);
        } else {
            boundingBoxes[x + y*width] = null;
        }
    }

    public Tile getTile(int x, int y){
        try{
            return Tile.tiles[tiles[x + y*width]];
        }catch (IndexOutOfBoundsException e){
            //e.printStackTrace();
            return null;
        }
    }

    public AABB getTileBoundingBox(int x, int y){
        try{
            return boundingBoxes[x + y*width];
        }catch (IndexOutOfBoundsException e){
            //e.printStackTrace();
            return null;
        }
    }

    @Deprecated //use global variable instead
    public int getScale(){
        return GlobalVariables.SCALE;
    }

    public Matrix4f getWorldProjectionMatrix(){
        return world;
    }
}

